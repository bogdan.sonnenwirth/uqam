#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// Types
// -----

// A resizable array
typedef struct {
    int *values;           // The values of the array
    unsigned int size;     // The current size
    unsigned int capacity; // The capacity
} array;

// Interface
// ---------

/**
 * Initialize an empty array
 *
 * @param a  The array to initialize
 */
void array_initialize(array *a);

/**
 * Append an element to the end of an array
 *
 * @param a  The array
 * @param e  The element
 */
void array_append(array *a, int e);

/**
 * Insert an element in an array
 *
 * @param a  The array
 * @param e  The element
 */
void array_insert(array *a, unsigned int i, int element);

/**
 * Remove an element from an array at a given index
 *
 * @param a  The array
 * @param i  The index of the element
 */
void array_remove(array *a, unsigned int i);

/**
 * Check if an array contains a given element
 *
 * @param a  The array
 * @param e  The element
 * @return   True if and only if the array contains the element
 */
bool array_has_element(const array *a, int e);

/**
 * Return the element at a given index in an array
 *
 * @param a  The array
 * @param i  The index
 */
int array_get(const array *a, unsigned int i);

/**
 * Print an array to stdout
 *
 * @param a  The array
 */
void array_print(const array *a);

/**
 * Delete an array
 *
 * @param a  The array 
 */
void array_delete(array *a);

/**
 * Check if index is valid
 *
 * If not, print an error to stderr and terminate the program.
 *
 * @param a  The array
 * @param i  The index
 */
void array_check_index(const array *a, unsigned int i);

// Help functions
// --------------

void array_resize_if_needed(array *a) {
    if (a->size == a->capacity) {
        a->capacity *= 2;
        a->values = realloc(a->values,
                            a->capacity * sizeof(int));
    }
}

int array_unsafe_get(const array *a, unsigned int i) {
    return a->values[i];
}

// Operations
// ----------

void array_initialize(array *a) {
    a->values = malloc(sizeof(int));
    a->size = 0;
    a->capacity = 1;
}

void array_append(array *a, int e) {
    array_resize_if_needed(a);
    a->values[a->size] = e;
    ++a->size;
}

void array_insert(array *a, unsigned int i, int e) {
    array_check_index(a, i);
    array_resize_if_needed(a);
    for (unsigned int j = a->size - 1; j > i; --j)
        a->values[j] = a->values[j - 1];
    a->values[i] = e;
    ++a->size;
}

void array_remove(array *a, unsigned int i) {
    array_check_index(a, i);
    ++i;
    while (i < a->size) {
        a->values[i - 1] = a->values[i];
        ++i;
    }
    --a->size;
}

bool array_has_element(const array *a, int e) {
    unsigned int i = 0;
    while (i < a->size && array_unsafe_get(a, i) != e)
         ++i;
    return i < a->size;
}

int array_get(const array *a, unsigned int i) {
    array_check_index(a, i);
    return array_unsafe_get(a, i);
}

void array_print(const array *a) {
    unsigned int i;
    printf("[");
    for (i = 0; i < a->size; ++i) {
        printf(" %d", a->values[i]);
    }
    printf(" ]");
}

void array_delete(array *a) {
    free(a->values);
}

void array_check_index(const array *a, unsigned int i) {
    if (i >= a->size) {
        fprintf(stderr, "Invalid index %d (size = %d)\n", i, a->size);
        exit(1);
    }
}

// Main
// ----
int main() {
    array a;
    array_initialize(&a);
    printf("Appending 3, 2, 5, 7, 8, 7: ");
    array_append(&a, 3);
    array_append(&a, 2);
    array_append(&a, 5);
    array_append(&a, 7);
    array_append(&a, 8);
    array_append(&a, 7);
    array_print(&a);
    printf("\nRemoving at position 2: ");
    array_remove(&a, 2);
    array_print(&a);
    printf("\nRemoving at position 4: ");
    array_remove(&a, 4);
    array_print(&a);
    printf("\nRemoving at position 2: ");
    array_remove(&a, 2);
    array_print(&a);
    printf("\nInserting 7 at position 1: ");
    array_insert(&a, 1, 7);
    array_print(&a);
    printf("\n");
    for (int e = 0; e <= 9; e += 2)
        printf("Has element %d ? %s\n", e,
               array_has_element(&a, e) ? "yes" : "no");
    array_delete(&a);
}
