#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// Types
// -----

struct queue_node {
    unsigned int value;      // Value in node
    struct queue_node *prev; // Previous node
    struct queue_node *next; // Next node
};

typedef struct {
    struct queue_node *first; // Pointer to first node
    struct queue_node *last;  // Pointer to last node
} queue;

// Interface
// ---------

/**
 * Initializes a queue
 *
 * Note: do not forget to call `queue_delete` when the queue is not needed
 * anymore.
 *
 * @return  The queue
 */
void queue_initialize(queue *q);

/**
 * Returns true if the queue is empty
 *
 * @param q  The queue to check
 * @return   true if and only if the queue is empty
 */
bool queue_is_empty(const queue *q);

/**
 * Pushes an element at the end of the queue
 *
 * @param q      The queue
 * @param value  The element to push
 */
void queue_push(queue *q, unsigned int value);

/**
 * Pops the first element of a queue
 *
 * The element is removed from the queue and returned.
 *
 * @param q  The queue
 * @return   The value of the top of the queue
 */
char queue_pop(queue *q);

/**
 * Prints the queue to stdout
 *
 * @param q  The queue
 */
void queue_print(const queue *q);

/**
 * Deletes a queue
 *
 * @param q  The queue to delete
 */
void queue_delete(queue *q);

// Implementation
// --------------

void queue_initialize(queue *q) {
    q->first = NULL;
    q->last  = NULL;
}

bool queue_is_empty(const queue *q) {
    return q->first  == NULL;
}

void queue_push(queue *q, unsigned int value) {
    struct queue_node *node = malloc(sizeof(struct queue_node));
    node->value = value;
    node->next = NULL;
    if (queue_is_empty(q)) q->first = node;
    else q->last->next = node;
    node->prev = q->last;
    q->last = node;
}

char queue_pop(queue *q) {
    // À compléter
}

void queue_print(const queue *q) {
    printf("[ ");
    for (struct queue_node *node = q->first;
         node != NULL;
         node = node->next) {
        printf("%d ", node->value);
    }
    printf("]");
}

void queue_delete(queue *q) {
    while (!queue_is_empty(q))
        queue_pop(q);
}

// Testing
// -------

int main(void) {
    queue q;
    printf("Creating empty queue: ");
    queue_initialize(&q);
    queue_print(&q);
    printf("\nPushing 4, 2, 1, 2: ");
    queue_push(&q, 4);
    queue_push(&q, 2);
    queue_push(&q, 1);
    queue_push(&q, 2);
    queue_print(&q);
    //printf("\nPopping %d: ", queue_pop(&q));
    //queue_print(&q);
    //printf("\nPushing 3, 1: ");
    //queue_push(&q, 3);
    //queue_push(&q, 1);
    //queue_print(&q);
    //while (!queue_is_empty(&q)) {
    //    printf("\nPopping %d: ", queue_pop(&q));
    //    queue_print(&q);
    //}
    //printf("\n");
    queue_delete(&q);
}
