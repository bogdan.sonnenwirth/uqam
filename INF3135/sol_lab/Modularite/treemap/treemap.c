#include "treemap.h"

// Auxiliary functions
// -------------------

/**
 * Return the node associated with a given key in a tree map
 *
 * Note: if the key does not exist in the tree map, return NULL.
 *
 * @param node  The current node
 * @param key   The key to search
 * @return      The node associated with the key
 */
struct tree_node *treemap_get_node(struct tree_node *node,
                                   const char *key);

/**
 * Insert a key-value node in a tree map
 *
 * @param node   The current node
 * @param key    The key to insert
 * @param value  The value to insert
 */
void treemap_insert_node(struct tree_node **node,
                         const char *key,
                         const char *value);

/**
 * Print the tree map starting from the given node
 *
 * @param node    The current node
 * @param prefix  The prefix to print for each line
 */
void treemap_print_node(const struct tree_node *node,
                        const char *prefix);

/**
 * Delete all nodes referenced by the given node
 *
 * @param node  The current node
 */
void treemap_delete_node(struct tree_node *node);

// Implementation
// --------------

void treemap_initialize(treemap *t) {
    t->root = NULL;
}

char *treemap_get(const treemap *t, const char *key) {
    struct tree_node *node = treemap_get_node(t->root, key);
    return node == NULL ? NULL : node->value;
}

struct tree_node *treemap_get_node(struct tree_node *node,
                                   const char *key) {
    if (node == NULL) {
        return NULL;
    } else {
        int cmp = strcmp(key, node->key);
        if (cmp == 0)
            return (struct tree_node*)node;
        else if (cmp < 0)
            return treemap_get_node(node->left, key);
        else
            return treemap_get_node(node->right, key);
    }
}

void treemap_set(treemap *t, const char *key, const char *value) {
    struct tree_node *node = treemap_get_node(t->root, key);
    if (node != NULL) {
        free(node->value);
        node->value = strdup(value);
    } else {
        treemap_insert_node(&(t->root), key, value);
    }
}

void treemap_insert_node(struct tree_node **node,
                         const char *key,
                         const char *value) {
    if (*node == NULL) {
        *node = malloc(sizeof(struct tree_node));
        (*node)->key = strdup(key);
        (*node)->value = strdup(value);
        (*node)->left = NULL;
        (*node)->right = NULL;
    } else if (strcmp(key, (*node)->key) < 0) {
        treemap_insert_node(&(*node)->left, key, value);
    } else {
        treemap_insert_node(&(*node)->right, key, value);
    }
}

bool treemap_has_key(const treemap *t, const char *key) {
    return treemap_get_node(t->root, key) != NULL;
}

void treemap_print(const treemap *t,
                   const char *prefix) {
    printf("%sTreeMap {\n", prefix);
    treemap_print_node(t->root, prefix);
    printf("%s}\n", prefix);
}

void treemap_print_node(const struct tree_node *node,
                        const char *prefix) {
    if (node != NULL) {
        treemap_print_node(node->left, prefix);
        printf("%s  %s: %s\n", prefix, node->key, node->value);
        treemap_print_node(node->right, prefix);
    }
}

void treemap_delete(treemap *t) {
    treemap_delete_node(t->root);
}

void treemap_delete_node(struct tree_node *node) {
    if (node != NULL) {
        treemap_delete_node(node->left);
        treemap_delete_node(node->right);
        free(node->key);
        free(node->value);
        free(node);
    }
}
