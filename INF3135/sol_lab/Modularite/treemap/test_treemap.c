#include "treemap.h"
#include <tap.h>

int main(void) {
    treemap t;
    treemap_initialize(&t);
    treemap_set(&t, "firstname", "Doina");
    treemap_set(&t, "lastname", "Precup");
    treemap_set(&t, "city", "Montreal");
    treemap_set(&t, "province", "Quebec");
    treemap_set(&t, "country", "Canada");
    treemap_set(&t, "position", "DeepMind Montreal Head");
    printf("# Printing the tree map\n");
    treemap_print(&t, "# ");
    is(treemap_get(&t, "firstname"), "Doina", "firstname is Doina");
    is(treemap_get(&t, "lastname"), "Precup", "lastname is Precup");
    is(treemap_get(&t, "position"), "DeepMind Montreal Head",
       "position is DeepMind Montreal Head");
    is(treemap_get(&t, "country"), "Canada", "country is Canada");
    printf("# Changing country to Romania\n");
    treemap_set(&t, "country", "Romania");
    is(treemap_get(&t, "country"), "Romania", "country is now Romania");
    printf("# Printing the tree map\n");
    treemap_print(&t, "# ");
    treemap_delete(&t);
}
