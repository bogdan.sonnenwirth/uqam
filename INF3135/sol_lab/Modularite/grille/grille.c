#include <stdio.h>
#include <stdlib.h>
#include <cairo.h>

#define USAGE "Usage: %s NUMROWS NUMCOLUMNS OFFSET\n"
#define FILENAME "grille.png"

void draw_grid(const char *filename,
               unsigned int r,
               unsigned int c,
               unsigned int offset) {
    unsigned int half_offset = offset / 2;
    printf("%s %d %d\n", filename, r, c);
    unsigned int surface_width = offset * (c + 1);
    unsigned int surface_height = offset * (r + 1);
    cairo_surface_t *output_image =
        cairo_image_surface_create(CAIRO_FORMAT_ARGB32,
                                   surface_width, surface_height);
    cairo_t *cr = cairo_create(output_image);
    cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
    cairo_rectangle(cr, 0, 0, surface_width, surface_height);
    cairo_fill(cr);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    for (unsigned int r2 = 0; r2 <= r; ++r2) {
        unsigned int y = offset * r2 + half_offset;
        cairo_move_to(cr, half_offset, y);
        cairo_line_to(cr, surface_width - half_offset, y);
    }
    for (unsigned int c2 = 0; c2 <= c; ++c2) {
        unsigned int x = offset * c2 + half_offset;
        cairo_move_to(cr, x, half_offset);
        cairo_line_to(cr, x, surface_height - half_offset);
    }
    cairo_stroke(cr);
    cairo_surface_write_to_png(output_image, filename);
    cairo_destroy(cr);
    cairo_surface_destroy(output_image);
}

int main(int argc, char *argv[]) {
    if (argc == 4) {
        draw_grid(FILENAME,
                  atoi(argv[1]),
                  atoi(argv[2]),
                  atoi(argv[3]));
    } else {
        fprintf(stderr, "Error: wrong number of arguments\n");
        fprintf(stderr, USAGE, argv[0]);
    }
}
