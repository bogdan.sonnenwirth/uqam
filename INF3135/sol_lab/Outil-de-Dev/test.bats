prog=words

@test "Chaîne vide" {
    run ./$prog ""
    [ "$status" -eq 0 ]
    [ "${lines[0]}" = "Analyzing the argument ''" ]
    [ "${lines[1]}" = "Is palindrome? yes" ]
    [ "${lines[2]}" = "Has double? no" ]
}

@test "La lettre 'a'" {
    run ./$prog a
    [ "$status" -eq 0 ]
    [ "${lines[0]}" = "Analyzing the argument 'a'" ]
    [ "${lines[1]}" = "Is palindrome? yes" ]
    [ "${lines[2]}" = "Has double? no" ]
}

@test "Le mot 'ressasser'" {
    run ./$prog ressasser
    [ "$status" -eq 0 ]
    [ "${lines[0]}" = "Analyzing the argument 'ressasser'" ]
    [ "${lines[1]}" = "Is palindrome? yes" ]
    [ "${lines[2]}" = "Has double? yes" ]
}

@test "Le mot 'aller'" {
    run ./$prog aller
    [ "$status" -eq 0 ]
    [ "${lines[0]}" = "Analyzing the argument 'aller'" ]
    [ "${lines[1]}" = "Is palindrome? no" ]
    [ "${lines[2]}" = "Has double? yes" ]
}

@test "Le mot 'AA'" {
    run ./$prog AA
    [ "$status" -eq 0 ]
    [ "${lines[0]}" = "Analyzing the argument 'AA'" ]
    [ "${lines[1]}" = "Is palindrome? yes" ]
    [ "${lines[2]}" = "Has double? yes" ]
}
