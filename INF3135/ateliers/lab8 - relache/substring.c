#include <stdio.h>
#include <string.h>

#define USAGE "\
Usage: %s WORD [-o FILENAME]\n\
Prints the Hasse diagram of WORD to the DOT format\n\
with respect to the subword relation.\n\
By default, prints to stdout.\n\
\n\
Option:\n\
  -o FILENAME    Writes output to FILENAME\n\
"

/**
 * Prints the given subword
 *
 * @param s     The reference word
 * @param i     The start index of the subword
 * @param n     The length of the subword
 */
void print_subword(const char *word,
                   unsigned int i,
                   unsigned int n) {
    if (n == 0)
        printf("empty");
    else
        printf("%.*s", n, word + i);
}

/**
 * Prints all Graphviz nodes
 *
 * @param word  The reference word
 */
void print_all_nodes(const char *word) {
    // À compléter
}

/**
 * Prints all Graphviz edges
 *
 * @param word  The reference word
 */
void print_all_edges(const char *word) {
    // À compléter
}

/**
 * Prints all Graphviz edges
 *
 * @param file  The stream on which to print
 * @param word  The reference word
 */
void print_graph(const char *word) {
    // À compléter
}

int main(int argc, char *argv[]) {
    // À compléter
}
