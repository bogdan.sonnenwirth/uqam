/*
 * pointeur.c
 * Author : Serge D
 */

#include <stdio.h>

int main(int argc, char **argv)
{
  int entier = 5;
  int *p1;
  int **p2;

  p1 = &entier;
  p2 = &p1;

  printf("entier : %d\n", entier);
  printf("*p1 : %d\n", *p1);
  printf("**p2 : %d\n", **p2);
    
  printf(" p1 : %p\n", p1);
  printf("*p2 : %p\n", *p2);
    

  return 0;
}

