#include <stdio.h>
#include <stdlib.h>

/* La fonction de comparaison doit avoir 
 * la signature (const void *, const void *)
 */
int compare_ints(const void *a, const void *b) {
    // On doit donc faire des conversions à l'intérieur
    return *(int*)a - *(int*)b;
}

int main(void) {
    int a[] = {8,3,4,2,0,5};
    qsort(a, 6, sizeof(int), compare_ints);
    for (unsigned int i = 0; i < 6; ++i)
        printf ("%d ", a[i]);
    return 0;
}
