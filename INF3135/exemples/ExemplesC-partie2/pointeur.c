/*
 * pointeur.c
 * Author : Serge D
 */
#include <stdio.h>

int main(int argc, char **argv)
{
  int entier = 12;
  int *p1;

  char caractere = 'A';
  char *p2;

  p1 = &entier;

  printf("Affichage du contenu de entier : %d\n", entier);
  printf("Affichage de l'adresse p1 de entier : %p\n", p1);
  printf("Affichage du contenu à l'adresse de p1 : %d\n\n", *p1);

  p2 = &caractere;

  printf("Affichage de caractere : %c\n", caractere);
  printf("Affichage de p2 : %p\n", p2);
  printf("Affichage du contenu à l'adresse de p2 : %c\n\n", *p2);

  *p2 = 'C';
  printf("Affichage de caractere après modification de la valeur contenue dans p2 : %c\n", caractere);

  (*p2)++;
  printf("Affichage de caractere après incrémentation de la valeur contenue dans p2 : %c\n\n", caractere);

  return 0;
}

