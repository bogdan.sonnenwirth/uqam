/*
 * menu.c
 * Author : Serge D
 */
#include <stdio.h>

int main(int argc, char **argv)
{
  int montant = 0;
  int choix;

  printf("1-Tempura de crevettes 6.00$\n");
  printf("2-Soupe Miso           3.00$\n");
  printf("Choisissez une entrée>");

  scanf("%d", &choix);

  if (choix == 1)
    montant += 6;
  else if (choix == 2)
    montant += 3;

  printf("1-Sashimis         24.00$\n");
  printf("2-Poulet au beurre 19.00$\n");
  printf("Choisissez un plat principal>");

  scanf("%d", &choix);

  if (choix == 1)
    montant += 24;
  else if (choix == 2)
    montant += 19;

  printf("1-Crème glacée napolitaine 1.00$\n");
  printf("2-Sorbet au durion         9.00$\n");
  printf("Choisissez un dessert>");

  scanf("%d", &choix);

  if (choix == 1)
    montant += 1;
  else if (choix == 2)
    montant += 9;

  printf("Montant total de la facture : %d.00$\n", montant);

  return 0;
}
