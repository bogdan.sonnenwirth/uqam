#include <stdio.h>

#define BUFFER_SIZE 20

int main(void) {
    char c1 = getchar(), c2 = getchar(), c3 = getchar();
    char line[BUFFER_SIZE];
    fgets(line, BUFFER_SIZE, stdin);
    printf("3 premiers caractères: %c %c %c\n", c1, c2, c3);
    printf("Reste de ligne: %s\n", line);
    return 0;
}
