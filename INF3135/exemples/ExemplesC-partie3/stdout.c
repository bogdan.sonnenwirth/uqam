#include <stdio.h>

int main(void) {
    char c = 'A';
    putchar(c);
    putchar('\n');
    puts("C est un langage particulier");
    fwrite("Incroyable!", sizeof(char), 4, stdout);
    printf("\n%s %c %p %lf\n",
           "Fantastique", '!', &c, 1.23456789);
    return 0;
}
