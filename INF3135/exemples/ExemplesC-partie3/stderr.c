#include <stdio.h>

int main(void) {
    char c = 'A';
    fputc(c, stderr);
    fputc('\n', stderr);
    fputs("C est un langage particulier", stderr);
    fwrite("Incroyable!", sizeof(char), 4, stderr);
    fprintf(stderr, "\n%s %c %p %lf\n",
           "Fantastique", '!', &c, 1.23456789);
    return 0;
}
