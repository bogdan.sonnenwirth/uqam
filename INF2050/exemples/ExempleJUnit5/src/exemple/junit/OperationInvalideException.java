package exemple.junit;

public class OperationInvalideException extends Exception {
    public OperationInvalideException(String message) {
        super(message);
    }
}
