package exemple.inf2050;

import java.io.FileNotFoundException;

import static java.lang.System.exit;


public class Main {

    public static void main(String[] args) throws FileNotFoundException, LabINF2050Exception {

        if (args.length != 1){
            System.out.println("nombre d'arguments invalide...");
            exit(1);
        }

        JSONHash obj = new JSONHash(args[0]);
        obj.load();

        // EXERCICE 1

        System.out.println("Le nombre d'albums : " + obj.countAlbum());

        System.out.println("Le nombre de singles : " + obj.countSingle());

        System.out.println("La liste des albums publiés depuis 2003 : " );
        obj.showAll(obj.getListAlbumFrom2003());

        System.out.println("La liste des albums avec une note de 5 : " );
        obj.showAll(obj.getListAlbumNote5());

        // EXERCICE 2

        System.out.println("La liste des 3 albums préferés : " );
        System.out.println(obj.getListAlbumPreferes());

    }
}
