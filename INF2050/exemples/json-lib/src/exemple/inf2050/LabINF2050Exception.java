package exemple.inf2050;


public class LabINF2050Exception extends Exception {

    public LabINF2050Exception(String msg) {
        super(msg);
    }
}