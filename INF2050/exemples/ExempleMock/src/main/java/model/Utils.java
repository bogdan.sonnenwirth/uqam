package model;

import java.io.IOException;

import static java.lang.System.exit;

public class Utils {

    public static void main(String [] args ) {

        final int NOMBRE_ARGUMENT = 2;
        if (args.length < NOMBRE_ARGUMENT) {
            System.out.println("Arguments invalides");
            exit(1);
        }
        FileWriterWrapper writer = new FileWriterWrapper(null);
        writeOnDisk(writer, "bonjour");
    }

    public static void writeOnDisk(FileWriterWrapper writer, String str){

        try {
            writer.write(str);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
