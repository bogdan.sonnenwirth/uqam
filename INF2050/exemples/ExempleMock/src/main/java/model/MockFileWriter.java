package model;

import java.io.IOException;

public class MockFileWriter extends FileWriterWrapper {

    private String writtenData = "";

    public MockFileWriter(){
        super(null);
    }

    @Override
    public void write(String str) throws IOException {
        writtenData = writtenData + str;
    }

    public String getWrittenData(){
        return writtenData;
    }
}
